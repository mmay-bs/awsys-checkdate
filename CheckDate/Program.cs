﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckDate
{
    class Program
    {
        static void Main(string[] args)
        {
            // Schleifen Status. Solange run true ist, wird ein neues Datum abgefragt.
            bool run = true;

            // Basis Parameter initialisieren
            string DateString;
            int Day;
            int Month;
            int Year;

            while (run)
            {
                writeHeader();

                Console.Write("   Bitte geben Sie das zu überprüfende Datum ein [Tag.Monat.Jahr]: ");
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                DateString = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.WriteLine("\n   Die Überprüfung des Datums ergab folgendes Ergebnis:\n");

                string[] DateParts = DateString.Split('.');

                Year = isValidYear(DateParts[2]); // 0 oder int Jahr
                Month = isValidMonth(DateParts[1]); // 0 oder int Monat
                Day = isValidDay(DateParts[0], Month, Year); // 0 oder int Tag

                // Das Datum ist korrekt wenn Day, Month, Year != 0 sind
                if (Year != 0 && Month != 0 && Day != 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("      Das von Ihnen eingegebene Datum ist gültig!");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("      Das von Ihnen eingegebene Datum ist nicht gültig!");
                }

                Console.ForegroundColor = ConsoleColor.Gray;

                Console.Write("\n\n\n   Möchten Sie ein weiteres Datum überprüfen? [j/n]: ");

                // Beende die Schleife (Schlussendlich das Programm) wenn etwas anderes als j eingegeben wird.
                if (Console.ReadLine() != "j")
                {
                    run = false;
                }
            }

            writeHeader();

            Console.Write("   Programm beendet! Fenster schließen mit beliebiger Taste. ");
            Console.ReadKey();
        }
        static int isValidDay(string DayString, int Month, int Year)
        {
            int MonthDays;
            int Day = Convert.ToInt32(DayString);

            switch (Month)
            { 
                case 2:
                    MonthDays = 28 + isLeapYear(Year);
                    break;

                case 1:
                case 3:
                case 5:
                case 7:
                case 10:
                case 12:
                    MonthDays = 31;
                    break;

                default:
                    MonthDays = 30;
                    break;
            }

            if (Day >= 1 && Day <= MonthDays)
            {
                return Day;
            }

            return 0;
        }

        static int isValidMonth(string MonthString)
        {
            int Month = Convert.ToInt32(MonthString);

            if (Month >= 1 && Month <= 12)
            {
                return Month;
            }

            return 0;
        }

        static int isLeapYear(int Year)
        {
            if(Year % 4 == 0 && (Year % 100 != 0 || Year % 400 == 0))
            {
                return 1;
            }

            return 0;
        }

        static int isValidYear(string YearString)
        {
            int Year = Convert.ToInt32(YearString);

            if (Year >= 1 && Year <= 9999)
            {
                return Year;
            }

            return 0;
        }
        static void writeHeader()
        {
            Console.Clear();

            if(Console.WindowWidth != 80)
            {
                Console.WindowWidth = 80;
            }

            if (Console.WindowHeight != 20)
            {
                Console.WindowHeight = 20;
            }

            Console.Write("################################################################################");
            Console.Write("#                                                                              #");
            Console.Write("#                               AWSYS - DateCheck                              #");
            Console.Write("#                                                                              #");
            Console.Write("################################################################################");
            Console.WriteLine();
        }
    }
}
